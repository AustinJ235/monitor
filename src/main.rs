use std::process::Command;

fn main() {
	let listmonitors = String::from_utf8(Command::new("xrandr").arg("--listmonitors").output().unwrap().stdout).unwrap();
	let mut monitors = Vec::new();
	
	for monitor_line in listmonitors.lines().rev() {
		if monitor_line.contains("fourk") {
			let mut by_white = monitor_line.split_whitespace().rev();
			monitors.push(by_white.next().unwrap().to_string());
			monitors.push(by_white.next().unwrap().to_string());
			let output = String::from_utf8(Command::new("xrandr").arg("--delmonitor").arg("fourk").output().unwrap().stdout).unwrap();
			println!("ran 'xrandr --delmonitor fourk', Output: '{}'", output);
		} else {
			monitors.push(monitor_line.split_whitespace().rev().next().unwrap().to_string());
		}
	}
	
	monitors.pop().unwrap();
	
	let xrandr = String::from_utf8(Command::new("xrandr").output().unwrap().stdout).unwrap();
	let connected_monitors: Vec<String> = xrandr.lines().rev().filter_map(|v| if v.contains(" connected") {
		v.split_whitespace().next().map(|v| v.to_string())
	} else {
		None
	}).collect();
	
	for name in &monitors {
		if !connected_monitors.contains(name) {
			let output = String::from_utf8(Command::new("xrandr").arg("--output").arg(name).arg("--off").output().unwrap().stdout).unwrap();
			println!("ran 'xrandr --output {} --off', Output: '{}'", name, output);
		}
	}
	
	let hr_output = String::from_utf8(
		Command::new("xrandr")
			.arg("--output").arg(&connected_monitors[2])
			.arg("--auto")
			.arg("--rate 239.76")
			.arg("--primary")
			.output().unwrap().stdout
	).unwrap();
	
	println!("ran 'xrandr --output {} --auto --rate 239.76 --primary', Output: '{}'", connected_monitors[2], hr_output);
	
	let ls_output = String::from_utf8(
		Command::new("xrandr")
			.arg("--output").arg(&connected_monitors[0])
			.arg("--auto")
			.arg("--right-of").arg(&connected_monitors[2])
			.output().unwrap().stdout
	).unwrap();
	
	println!("ran 'xrandr --output {} --auto --right-of {}', Output '{}'", connected_monitors[0], connected_monitors[2], ls_output);
	
	let rs_output = String::from_utf8(
		Command::new("xrandr")
			.arg("--output").arg(&connected_monitors[1])
			.arg("--auto")
			.arg("--right-of").arg(&connected_monitors[0])
			.output().unwrap().stdout
	).unwrap();
	
	println!("ran 'xrandr --output {} --auto --right-of {}', Output '{}'", connected_monitors[1], connected_monitors[0], rs_output);
	
	let fourk_output = String::from_utf8(
		Command::new("xrandr")
			.arg("--setmonitor").arg("fourk")
			.arg("auto")
			.arg(format!("{},{}", connected_monitors[0], connected_monitors[1]))
			.output().unwrap().stdout
	).unwrap();
	
	println!("ran 'xrandr --setmonitor fourk auto {},{}', Output'{}'", connected_monitors[0], connected_monitors[1], fourk_output);
	Command::new("nitrogen").arg("--restore").output().unwrap();
}

